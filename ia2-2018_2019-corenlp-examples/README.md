Esempi Stanford CoreNLP
=======================

Questo progetto contiene esempi relativi all'uso di Stanford CoreNLP (https://stanfordnlp.github.io/CoreNLP/).

Gli esempi sono grosso modo quelli forniti sul sito ufficiale di CoreNLP (vedi https://stanfordnlp.github.io/CoreNLP/api.html).

CoreNLP può anche essere usato a [riga di comando](https://stanfordnlp.github.io/CoreNLP/cmdline.html) o
come un [server](https://stanfordnlp.github.io/CoreNLP/corenlp-server.html).

Nel caso del deployment come server, non è necessario invocare esplicitamente i suoi servizi HTTP: in fondo
alla relativa pagina di guida, viene mostrato un client Java (la cui API coincide con quella normalmente usata
nel deployment locale).

package it.uniroma2.art.teaching.ia2_2018_2019;

import java.util.Properties;

import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;

public class CoreNLPWrapperExample {
	public static void main(String[] args) {
		// Utilizza un oggetto Properties per configurare la pipeline ("annotators") e le proprietà dei
		// singoli annotatori (annotatore.proprietà)
		Properties props = new Properties();
		// Annotatori da eseguire
		props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,depparse,coref");

		// Nella barra laterale del sito, c'è una sezione dedicata agli annotatori, per ciscuno dei quali
		// vengono indicate diverse informazioni, tra cui le annotazioni prodotte e dipendenze da altri
		// annotatori

		// Costruisce la pipeline, servendosi della configurazione stabilita in precendenza
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		// Crea un'annotazione contente il testo da annotare
		CoreDocument doc = new CoreDocument("Albert Einstein was a 20th century physicist. "
				+ "He developed the theory of relativity. " + " He also won the Nobel Prize.");

		// Annota il documento
		pipeline.annotate(doc);

		// Per ogni frase
		for (CoreSentence s : doc.sentences()) {
			// Stampa i token (con lemma e POS)
			s.tokens().forEach(
					t -> System.out.print("(" + t.tag() + "/" + t.lemma() + "/" + t.word() + ")"));
			System.out.println();

			// Stampa le dipendenze
			SemanticGraph depParse = s.dependencyParse();

			System.out.println(depParse);

		}

		// Stampa le coreferenze
		System.out.println(doc.corefChains());

	}
}

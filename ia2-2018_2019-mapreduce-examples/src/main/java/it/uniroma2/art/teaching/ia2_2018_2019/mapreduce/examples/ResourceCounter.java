package it.uniroma2.art.teaching.ia2_2018_2019.mapreduce.examples;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class ResourceCounter {

	public static class QuadReaderMapper extends Mapper<Object, Text, Text, IntWritable> {

		public static enum CountersEnum {
			INPUT_RESOURCES
		}

		public static final IntWritable ONE = new IntWritable(1);

		private Text resourceString = new Text();

		private boolean countNamespaces;

		@Override
		protected void setup(Mapper<Object, Text, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {
			countNamespaces = context.getConfiguration().getBoolean("resourcecount.countnamespaces", false);
		}

		@Override
		protected void map(Object key, Text value, Mapper<Object, Text, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {
			Model model = Rio.parse(new StringReader(value.toString()), "", RDFFormat.NQUADS);

			Statement stmt = model.iterator().next();

			List<IRI> iris = Arrays.asList(stmt.getSubject(), stmt.getPredicate(), stmt.getObject()).stream()
					.filter(IRI.class::isInstance).map(IRI.class::cast).collect(Collectors.toList());

			for (IRI aIRI : iris) {
				resourceString.set(countNamespaces ? aIRI.getNamespace() : aIRI.stringValue());
				context.write(resourceString, new IntWritable(1));

				Counter counter = context.getCounter(CountersEnum.INPUT_RESOURCES);
				counter.increment(1);
			}
		}
	}

	public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

		private IntWritable result = new IntWritable();

		@Override
		protected void reduce(Text key, Iterable<IntWritable> values,
				Reducer<Text, IntWritable, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key, result);
		}

	}

	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration conf = new Configuration();
		GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
	    String[] remainingArgs = optionParser.getRemainingArgs();
		Job job = Job.getInstance(conf, "resource count");
		job.setJarByClass(ResourceCounter.class);
		job.setMapperClass(QuadReaderMapper.class);
		job.setReducerClass(IntSumReducer.class);
		job.setCombinerClass(IntSumReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(remainingArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(remainingArgs[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}

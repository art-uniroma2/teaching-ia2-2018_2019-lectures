package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class Exercise1Alt {
	public static void main(String[] args) {
		/*
		 * Soluzione alternativa basata sulla fluent API ModelBuilder. Da notare che i vari metodi della
		 * classe ModelBuilder restituiscono un riferimento all'oggetto corrente in modo che le varie chiamate
		 * di metodo possono essere concatenate.
		 * 
		 * I tag @formatter:off|on devono essere attivati nelle preferenze del formattatore del codice Java
		 */

		ValueFactory vf = SimpleValueFactory.getInstance();

		// @formatter:off
		
		Model model = new ModelBuilder(new TreeModel())
			.setNamespace("ex", "http://example.org/resource/")
			.setNamespace("ont", "http://example.org/ontology#")
			.setNamespace(OWL.NS)
			.setNamespace(RDFS.NS)
			.subject("ex:socrates")
				.add(RDF.TYPE, "ont:Person")
				.add(RDFS.LABEL, vf.createLiteral("Socrates", "en"))
			.namedGraph("http://example.org/ontology")
				.subject("ont:Person")
					.add(RDF.TYPE, OWL.CLASS)
					.add(RDFS.LABEL, vf.createLiteral("person", "en"))
					.add(RDFS.SUBCLASSOF, "ont:Mortal")
				.subject("ont:Mortal")
					.add(RDF.TYPE, OWL.CLASS)
					.add(RDFS.LABEL, vf.createLiteral("mortal", "en")).build();

		// @formatter:on

		System.out.println("--- Content of the Model ---");
		Rio.write(model, System.out, RDFFormat.TRIG);
	}

}

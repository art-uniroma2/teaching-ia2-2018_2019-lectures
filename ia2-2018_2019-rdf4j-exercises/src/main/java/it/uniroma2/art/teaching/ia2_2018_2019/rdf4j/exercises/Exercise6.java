package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;

public class Exercise6 {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		URL inputURL = Exercise6.class.getResource("exercise6.ttl");
		try (InputStream is = inputURL.openStream();
				OutputStream os = new FileOutputStream("exercise6.rdf")) {
			RDFWriter writer = Rio.createWriter(RDFFormat.RDFXML, os);
			RDFParser reader = Rio.createParser(RDFFormat.TURTLE);
			reader.setRDFHandler(writer);
			reader.parse(is, inputURL.toExternalForm());
		}
	}

}

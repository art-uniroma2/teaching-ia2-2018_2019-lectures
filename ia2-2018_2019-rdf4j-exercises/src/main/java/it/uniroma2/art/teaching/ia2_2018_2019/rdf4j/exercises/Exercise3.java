package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.server.SocketSecurityException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

public class Exercise3 {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		/*
		 * Soluzione alternativa basata sulla fluent API ModelBuilder. Da notare che i vari metodi della
		 * classe ModelBuilder restituiscono un riferimento all'oggetto corrente in modo che le varie chiamate
		 * di metodo possono essere concatenate.
		 * 
		 * I tag @formatter:off|on devono essere attivati nelle preferenze del formattatore del codice Java
		 */

		ValueFactory vf = SimpleValueFactory.getInstance();

		// @formatter:off
		
		Model model = new ModelBuilder(new TreeModel())
			.setNamespace("ex", "http://example.org/resource/")
			.setNamespace("ont", "http://example.org/ontology#")
			.setNamespace(OWL.NS)
			.setNamespace(RDFS.NS)
			.subject("ex:socrates")
				.add(RDF.TYPE, "ont:Person")
				.add(RDFS.LABEL, vf.createLiteral("Socrates", "en"))
			.namedGraph("http://example.org/ontology")
				.subject("ont:Person")
					.add(RDF.TYPE, OWL.CLASS)
					.add(RDFS.LABEL, vf.createLiteral("person", "en"))
					.add(RDFS.SUBCLASSOF, "ont:Mortal")
				.subject("ont:Mortal")
					.add(RDF.TYPE, OWL.CLASS)
					.add(RDFS.LABEL, vf.createLiteral("mortal", "en")).build();

		// @formatter:on

		System.out.println("--- Content of the Model ---");
		Rio.write(model, System.out, RDFFormat.TRIG);

		File fileTriG = new File("exercise3.trig");
		try (FileOutputStream out = new FileOutputStream(fileTriG)) {
			Rio.write(model, out, RDFFormat.TRIG);
		}

		File fileTTL = new File("exercise3.ttl");
		try (FileOutputStream out = new FileOutputStream(fileTTL)) {
			Rio.write(model, out, RDFFormat.TURTLE);
		}

		// Possiamo osservare che nel file Turtle si è persa l'informazione sui grafi di appartenenza delle
		// triple

		try (FileInputStream is = new FileInputStream(fileTriG)) {
			Model modelFromTriG = Rio.parse(is, fileTriG.toURI().toString(), RDFFormat.TRIG);
			System.out.println("Is the Model read from the TriG file equal to the source Model? "
					+ modelFromTriG.equals(model));
		}

		try (FileInputStream is = new FileInputStream(fileTTL)) {
			Model modelFromTTL = Rio.parse(is, fileTTL.toURI().toString(), RDFFormat.TURTLE);
			System.out.println("Is the Model read from the Turtle file equal to the source Model? "
					+ modelFromTTL.equals(model));
		}

		try (FileInputStream is = new FileInputStream(fileTTL)) {
			QuadCounter quadCounter = new QuadCounter();

			RDFParser parser = Rio.createParser(RDFFormat.TURTLE);
			parser.setRDFHandler(quadCounter);

			parser.parse(is, fileTTL.getAbsolutePath());
			System.out.println("Number of triples in the Tutle file: " + quadCounter.getCounter());
		}

	}

	public static class QuadCounter extends AbstractRDFHandler {

		private long counter = 0;

		@Override
		public void startRDF() throws RDFHandlerException {
			counter = 0;
		}

		@Override
		public void handleStatement(Statement st) throws RDFHandlerException {
			counter++;
		}

		public long getCounter() {
			return counter;
		}
	}

	// Se volessi ignorare quadruple duplicate, dovrei accumulare tutte le qudruple in un Set così da
	// eliminare i duplicati
	public static class UniqueQuadCounter extends AbstractRDFHandler {

		private Set<Statement> uniqueStatements = new LinkedHashSet<>();

		@Override
		public void startRDF() throws RDFHandlerException {
			uniqueStatements.clear();
		}

		@Override
		public void handleStatement(Statement st) throws RDFHandlerException {
			uniqueStatements.add(st);
		}

		public long getCounter() {
			return uniqueStatements.size();
		}
	}

}

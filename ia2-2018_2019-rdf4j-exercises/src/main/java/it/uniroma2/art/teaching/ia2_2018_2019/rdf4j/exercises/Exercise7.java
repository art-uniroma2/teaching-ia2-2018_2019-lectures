package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

public class Exercise7 {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Repository rep = new SailRepository(new ForwardChainingRDFSInferencer(new MemoryStore()));
		rep.initialize();
		try {
			try (RepositoryConnection conn = rep.getConnection()) {
				conn.add(Exercise7.class.getResource("exercise7.trig"), null, RDFFormat.TRIG);
				ValueFactory vf = conn.getValueFactory();
				IRI socrates = vf.createIRI("http://example.org/resource/socrates");
				IRI mortal = vf.createIRI("http://example.org/ontology#Mortal");
				System.out.println("Is Socrates mortal? (explicit only) "
						+ conn.hasStatement(socrates, RDF.TYPE, mortal, false));
				System.out.println("Is Socrates mortal? (inferred included) "
						+ conn.hasStatement(socrates, RDF.TYPE, mortal, true));
			}
		} finally {
			rep.shutDown();
		}
	}

}

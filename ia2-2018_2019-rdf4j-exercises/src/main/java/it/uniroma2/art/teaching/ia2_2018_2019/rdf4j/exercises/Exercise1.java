package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class Exercise1 {
	public static void main(String[] args) {
		String exNS = "http://example.org/resource/";
		String ontNS = "http://example.org/ontology#";
		
		Model model = new TreeModel();
		model.setNamespace("ex", exNS);
		model.setNamespace("ont", ontNS);
		model.setNamespace(OWL.NS);
		model.setNamespace(RDFS.NS);
		
		ValueFactory vf = SimpleValueFactory.getInstance();
		
		IRI socrates = vf.createIRI(exNS, "socrates");
		IRI person = vf.createIRI(ontNS, "Person");
		IRI mortal = vf.createIRI(ontNS, "Mortal");
		
		IRI ontGraph = vf.createIRI("http://example.org/ontology");
		
		model.add(socrates, RDF.TYPE, person);
		model.add(socrates, RDFS.LABEL, vf.createLiteral("Socrates", "en"));
		
		model.add(person, RDF.TYPE, OWL.CLASS, ontGraph);
		model.add(person, RDFS.LABEL, vf.createLiteral("person", "en"), ontGraph);
		model.add(person, RDFS.SUBCLASSOF, mortal, ontGraph);
		
		model.add(mortal, RDF.TYPE, OWL.CLASS, ontGraph);
		model.add(mortal, RDFS.LABEL, vf.createLiteral("mortal", "en"), ontGraph);
		
		System.out.println("--- Content of the Model ---");
		Rio.write(model, System.out, RDFFormat.TRIG);
	}

}

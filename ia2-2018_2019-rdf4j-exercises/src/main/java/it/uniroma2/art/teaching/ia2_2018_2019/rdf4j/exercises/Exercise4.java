package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.WriterConfig;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;

public class Exercise4 {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		// @formatter:off
		String inputFragment = 
			"@prefix ont: <http://example.org/ontology#> .                                    \n" +
			"@prefix owl: <http://www.w3.org/2002/07/owl#> .                                  \n" +
	        "                                                                                 \n" +
			"ont:AmphibianVehile owl:intersectionOf (ont:LandVehicle ont:AquaticVehicle) .    \n";
		// @formatter:on

		System.out.println("--- Input Turtle fragment ---");
		System.out.println(inputFragment);
		Model inputModel = Rio.parse(new StringReader(inputFragment), "", RDFFormat.TURTLE);

		System.out.println("--- Output in Turtle ---");
		Rio.write(inputModel, System.out, RDFFormat.TURTLE);

		System.out.println("--- Output in Turtle with bnode inlining ---");
		WriterConfig config = new WriterConfig();
		config.set(BasicWriterSettings.INLINE_BLANK_NODES, true);
		Rio.write(inputModel, System.out, RDFFormat.TURTLE, config);
	}

}

package it.uniroma2.art.teaching.ia2_2018_2019.rdf4j.exercises;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class Exercise5 {
	public static void main(String[] args) throws IOException {
		// @formatter:off
		String inputFragment = 
			"@prefix ont: <http://example.org/ontology#> .                                    \n" +
			"@prefix owl: <http://www.w3.org/2002/07/owl#> .                                  \n" +
	        "                                                                                 \n" +
			"ont:AmphibianVehile owl:intersectionOf (ont:LandVehicle ont:AquaticVehicle) .    \n";
		// @formatter:on

		System.out.println("--- Input Turtle fragment ---");
		Model inputModel1 = Rio.parse(new StringReader(inputFragment), "", RDFFormat.TURTLE);
		Model inputModel2 = Rio.parse(new StringReader(inputFragment), "", RDFFormat.TURTLE);

		// Da notare che nei due Model sono usati bnode identifier divers

		System.out.println("---");

		System.out.println("Input Model 1 = " + inputModel1);
		System.out.println("Input Model 2 = " + inputModel2);

		System.out.println("Is inputModel1 = inputModel2? " + inputModel1.equals(inputModel2));

		/*
		 * Nella classe AbstractModel troviamo la seguente implementazione del metodo Object.equals(..) 
		 * che usa il metodo Models.isomorphic(...) per valutare l'uguaglianza di due Model in termini di 
		 * isomorfismo tra grafi.
		 * //@formatter:off
		 * 	@Override
		 *	public boolean equals(Object o) {
		 *		if (this == o) {
		 *			return true;
		 *		}
		 *		if (o instanceof Model) {
		 *			Model model = (Model)o;
		 *			return Models.isomorphic(this, model);
		 *		}
		 *		return false;
		 *	}
		 * //@formatter:on
		 */

	}

}

Esercizi su Eclipse RDF4J
=========================

Questo progetto contiene alcuni esercizi svolti relativi a [Eclipse RDF4J](http://rdf4j.org/).

**Attenzione**: questo file README è scritto in sintassi Markdown, pertanto alcuni simboli e l'identazione di
certe righe sono finalizzati all'impaginazione.

Esercizio 1
-----------

Costruire un oggetto `Model` contenente il seguente dataset (espresso in sintassi TriG)

	@prefix ex: <http://example.org/resource/> .
	@prefix ont: <http://example.org/ontology#> .
	@prefix owl: <http://www.w3.org/2002/07/owl#> .
	@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
	
	<http://example.org/ontology> {
	  ont:Mortal a owl:Class;
	    rdfs:label "mortal"@en .
	  
	  ont:Person a owl:Class;
	    rdfs:label "person"@en;
	    rdfs:subClassOf ont:Mortal .
	}
	
	{
	  ex:socrates a ont:Person;
	    rdfs:label "Socrates"@en .
	}
	
Esercizio 2
-----------

Utilizzando l'oggetto `Model` costruito nell'esercizio 1, ottenere:
1. tutte le triple col predicato `rdfs:label`;
2. le label della risorsa `ex:socrates`;
3. una mappa contenente le RDFS label di ciascuna risorsa (con almeno una label).

Esercizio 3
-----------

Utilizzando ancora l'oggetto `Model` costruito nell'esercizio 1:
1. scrivere il contenuto del Model in un file TriG;
2. scrivere il contenuto del Model in un file Turtle (cosa cambia?);
3. leggere il file TriG come un secondo Model, e verificare (usando il metodo `equals`) che sia identico al
model di partenza. Cosa succede, invece, leggendo il file Turtle, e perché?
3. Contare il numero di quadruple (compresi duplicati) nel file (aiuto: implementare un `RDFHandler`);
4. Cosa cambierebbe se uno volesse ignorare possibili quadruple duplicate (nel file di input)?

Esercizio 4
-----------

Usando Rio, si legga il seguente frammento in sintassi Turtle come un oggetto `Model`:

	@prefix ont: <http://example.org/ontology#> .
	@prefix owl: <http://www.w3.org/2002/07/owl#> .
	
	ont:AmphibianVehile owl:intersectionOf (ont:LandVehicle ont:AquaticVehicle) .

Si provi a scriverlo nuovamente in sintassi Turtle (va bene anche su console).

Notare che per la RDF Collection non viene usata la sintassi abbreviata. Risolvere il problema aggiugendo i 
`BasicWriterSettings` appropriati.

Esercizio 5
-----------

Si legga lo stesso frammento Turtle dell'esercizio 4 per due volte, costruendo due oggetti `Model`:
1. Stampare il contenuto dei due `Model` e verificare che in essi sono usati identicatori diversi per il bnode;
2. I due `Model` risultano uguali usando il metodo `Object.equals(Object o)`. Come mai?
 
Esercizio 6
-----------

Usando un `RDFParser` e un `RDFWriter`, scrivere un programma che legge un file Turtle e ne riversa il
contenuto in un file RDF/XML senza costruire un oggetto `Model` temporaneo.

Esercizio 7
-----------

Creare un repository embedded (`SailRepository`) in memoria e non persistente con inferenza RDFS.
Caricare nel repository un file contenente il dataset dell'esercizio 1.

Domandare al repository se `ex:socrates` è un `ont:Mortal` (con e senza inferenza)

 
 

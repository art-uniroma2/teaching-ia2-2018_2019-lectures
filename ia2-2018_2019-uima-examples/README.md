Esempi UIMA
===========

Questo repository contiene codice di esempio relativo all'uso di UIMA e di altri framework/progetti ad esso
collegati.

* [Apache UIMA](https://uima.apache.org/): un framework per lo sviluppo di sistemi di analisi di informazione
  non struttuata multi-modale
* [Apache uimaFIT](https://uima.apache.org/uimafit.html): un framework che semplifica l'utilizzo di Apache UIMA
  evitando quasi per intero la necessità di scrivere file di configurazion XML
* [DKpro Core](https://dkpro.github.io/dkpro-core/): componenti NLP wrappati come componenti UIMA
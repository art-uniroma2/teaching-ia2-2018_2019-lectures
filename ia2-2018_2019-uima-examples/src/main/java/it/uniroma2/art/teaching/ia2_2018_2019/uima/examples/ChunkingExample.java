package it.uniroma2.art.teaching.ia2_2018_2019.uima.examples;

import java.io.IOException;
import java.util.Collection;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.api.syntax.type.chunk.Chunk;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunker;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;

public class ChunkingExample {
	public static void main(String[] args) throws UIMAException, SAXException, IOException {
		JCas jcas = JCasFactory.createJCas();
		jcas.setDocumentText("Faust made a deal with the devil");
		jcas.setDocumentLanguage("en");

		AnalysisEngineDescription segmenterDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordSegmenter.class);

		AnalysisEngineDescription postaggerDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordPosTagger.class);

		AnalysisEngineDescription chunkerDescr = AnalysisEngineFactory
				.createEngineDescription(OpenNlpChunker.class);

		SimplePipeline.runPipeline(jcas, segmenterDescr, postaggerDescr, chunkerDescr);

		System.out.println("--- tokens ---");

		Collection<Token> tokens = JCasUtil.select(jcas, Token.class);
		for (Token tok : tokens) {
			System.out.println(tok.getBegin() + "-" + tok.getEnd() + "\t" + tok.getPos().getPosValue() + "\t"
					+ tok.getCoveredText());
		}

		System.out.println("--- chunks ---");

		Collection<Chunk> chunks = JCasUtil.select(jcas, Chunk.class);

		for (Chunk chunk : chunks) {
			System.out.println(chunk.getBegin() + "-" + chunk.getEnd() + "\t" + chunk.getChunkValue() + "\t"
					+ chunk.getCoveredText());
		}

	}

}



/* First created by JCasGen Thu Nov 22 18:37:17 CET 2018 */
package it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Thu Nov 22 18:37:17 CET 2018
 * XML source: C:/Users/Manuel/WORK/ART/Workspace/Eclipse/ia2-2018_2019-uima-examples/src/main/resources/it/uniroma2/art/teaching/ia2_2018_2019/uima/examples/reverbstyle/tuples.xml
 * @generated */
public class Tuple extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Tuple.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Tuple() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Tuple(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Tuple(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Tuple(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: arg1

  /** getter for arg1 - gets 
   * @generated
   * @return value of the feature 
   */
  public Argument getArg1() {
    if (Tuple_Type.featOkTst && ((Tuple_Type)jcasType).casFeat_arg1 == null)
      jcasType.jcas.throwFeatMissing("arg1", "it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple");
    return (Argument)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Tuple_Type)jcasType).casFeatCode_arg1)));}
    
  /** setter for arg1 - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setArg1(Argument v) {
    if (Tuple_Type.featOkTst && ((Tuple_Type)jcasType).casFeat_arg1 == null)
      jcasType.jcas.throwFeatMissing("arg1", "it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple");
    jcasType.ll_cas.ll_setRefValue(addr, ((Tuple_Type)jcasType).casFeatCode_arg1, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: arg2

  /** getter for arg2 - gets 
   * @generated
   * @return value of the feature 
   */
  public Argument getArg2() {
    if (Tuple_Type.featOkTst && ((Tuple_Type)jcasType).casFeat_arg2 == null)
      jcasType.jcas.throwFeatMissing("arg2", "it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple");
    return (Argument)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Tuple_Type)jcasType).casFeatCode_arg2)));}
    
  /** setter for arg2 - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setArg2(Argument v) {
    if (Tuple_Type.featOkTst && ((Tuple_Type)jcasType).casFeat_arg2 == null)
      jcasType.jcas.throwFeatMissing("arg2", "it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple");
    jcasType.ll_cas.ll_setRefValue(addr, ((Tuple_Type)jcasType).casFeatCode_arg2, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: relation

  /** getter for relation - gets 
   * @generated
   * @return value of the feature 
   */
  public Relation getRelation() {
    if (Tuple_Type.featOkTst && ((Tuple_Type)jcasType).casFeat_relation == null)
      jcasType.jcas.throwFeatMissing("relation", "it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple");
    return (Relation)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Tuple_Type)jcasType).casFeatCode_relation)));}
    
  /** setter for relation - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setRelation(Relation v) {
    if (Tuple_Type.featOkTst && ((Tuple_Type)jcasType).casFeat_relation == null)
      jcasType.jcas.throwFeatMissing("relation", "it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple");
    jcasType.ll_cas.ll_setRefValue(addr, ((Tuple_Type)jcasType).casFeatCode_relation, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    
package it.uniroma2.art.teaching.ia2_2018_2019.uima.examples;

import org.apache.uima.tools.docanalyzer.DocumentAnalyzer;

public class GUI {
	public static void main(String[] args) {
		DocumentAnalyzer.main(args);
	}
}

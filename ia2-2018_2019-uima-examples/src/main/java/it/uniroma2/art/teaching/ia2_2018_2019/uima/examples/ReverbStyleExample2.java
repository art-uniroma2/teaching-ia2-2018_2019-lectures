package it.uniroma2.art.teaching.ia2_2018_2019.uima.examples;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunker;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;

public class ReverbStyleExample2 {
	public static void main(String[] args) throws UIMAException, SAXException, IOException {
		JCas jcas = JCasFactory.createJCas();
		jcas.setDocumentText("Faust made a deal with the devil");
		jcas.setDocumentLanguage("en");

		AnalysisEngineDescription segmenterDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordSegmenter.class);

		AnalysisEngineDescription postaggerDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordPosTagger.class);

		AnalysisEngineDescription chunkerDescr = AnalysisEngineFactory
				.createEngineDescription(OpenNlpChunker.class);

		AnalysisEngineDescription reverbStyleDescr = AnalysisEngineFactory
				.createEngineDescription(ReVerbStyleAnnotator.class);

		AnalysisEngineDescription aggregateAnalysisEngine = AnalysisEngineFactory.createEngineDescription(
				Arrays.asList(segmenterDescr, postaggerDescr, chunkerDescr, reverbStyleDescr),
				Arrays.asList("segmenter", "postagger", "chunker", "reverb"), null, null, null);

		try (Writer w = new FileWriter("aggregate.xml")) {
			aggregateAnalysisEngine.toXML(w);
		}
	}

}

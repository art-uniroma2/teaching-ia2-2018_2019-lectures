package it.uniroma2.art.teaching.ia2_2018_2019.uima.examples;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.syntax.type.chunk.NC;
import it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Argument;
import it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Relation;
import it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple;

/*
 * V | V P | VW*P
 * V = verb particle? adv?
 * W = (noun | adj | adv | pron | det)
 * P = (prep | particle | inf. marker)
 *
 */

@TypeCapability(inputs = { "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
		"de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
		"de.tudarmstadt.ukp.dkpro.core.api.syntax.type.chunk.Chunk" }, outputs = {
				"it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple",
				"it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Argument",
				"it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Relation" })
public class ReVerbStyleAnnotator extends JCasAnnotator_ImplBase {

	private static final String V = "(%(VB|VBD|VBZ)%(%RP%)?(%RB%)?)";
	private static final String W = "(%(NN|NNS|NNP|NNPS|JJ|RB|PRP|DT)%)";
	private static final String P = "(%(IN|RP|TO)%)";
	private static final Pattern relationPattern = Pattern.compile(V + P + "|" + V + W + "*" + P + "|" + V);

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		for(Sentence sentence : JCasUtil.select(aJCas, Sentence.class)) {
			List<POS> posColl = new ArrayList<>(JCasUtil.selectCovered(aJCas, POS.class, sentence));

			String joined = posColl.stream().map(pos -> "%" + pos.getPosValue() + "%")
					.collect(Collectors.joining());

			Matcher matcher = relationPattern.matcher(joined);

			while (matcher.find()) {
				String matched = matcher.group(0);
				int matchStart = matcher.start();
				int matchEnd = matcher.end();

				String previous = joined.substring(0, matchStart);
				int relPosOffset = StringUtils.countMatches(previous, "%") / 2;
				int relPosLength = StringUtils.countMatches(matched, "%") / 2;

				POS beginPOS = posColl.get(relPosOffset);
				POS lastPOSIncluded = posColl.get(relPosOffset + relPosLength - 1);

				int begin = beginPOS.getBegin();
				int end = lastPOSIncluded.getEnd();

				Relation relation = new Relation(aJCas);
				relation.setBegin(begin);
				relation.setEnd(end);
				relation.addToIndexes();
			
				NC arg1Chunk = JCasUtil.selectSingleRelative(aJCas, NC.class, relation, -1);
				NC arg2Chunk = JCasUtil.selectSingleRelative(aJCas, NC.class, relation, +1);

				if (arg1Chunk == null || arg2Chunk == null) continue;
				
				Argument arg1 = new Argument(aJCas);
				arg1.setBegin(arg1Chunk.getBegin());
				arg1.setEnd(arg1Chunk.getEnd());
				arg1.addToIndexes();
				
				Argument arg2 = new Argument(aJCas);
				arg2.setBegin(arg2Chunk.getBegin());
				arg2.setEnd(arg2Chunk.getEnd());
				arg2.addToIndexes();
				
				Tuple tuple = new Tuple(aJCas);
				tuple.setBegin(arg1.getBegin());
				tuple.setEnd(arg2.getEnd());
				tuple.setArg1(arg1);
				tuple.setArg2(arg2);
				tuple.setRelation(relation);
				tuple.addToIndexes();
			}
				
		}
	}

}

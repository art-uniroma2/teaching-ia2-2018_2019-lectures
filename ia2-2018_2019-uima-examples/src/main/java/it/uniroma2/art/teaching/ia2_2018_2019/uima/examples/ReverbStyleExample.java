package it.uniroma2.art.teaching.ia2_2018_2019.uima.examples;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunker;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Argument;
import it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Relation;
import it.uniroma2.art.teaching.ia2_2018_2019.uima.examples.reverbstyle.Tuple;

public class ReverbStyleExample {
	public static void main(String[] args) throws UIMAException, SAXException, IOException {
		JCas jcas = JCasFactory.createJCas();
		jcas.setDocumentText("Faust made a deal with the devil");
		jcas.setDocumentLanguage("en");

		AnalysisEngineDescription segmenterDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordSegmenter.class);

		AnalysisEngineDescription postaggerDescr = AnalysisEngineFactory
				.createEngineDescription(StanfordPosTagger.class);

		AnalysisEngineDescription chunkerDescr = AnalysisEngineFactory
				.createEngineDescription(OpenNlpChunker.class);

		AnalysisEngineDescription reverbStyleDescr = AnalysisEngineFactory
				.createEngineDescription(ReVerbStyleAnnotator.class);

		SimplePipeline.runPipeline(jcas, segmenterDescr, postaggerDescr, chunkerDescr, reverbStyleDescr);

		for (Tuple tuple : JCasUtil.select(jcas, Tuple.class)) {
			System.out.println(tuple.getCoveredText());
			StringBuilder decoration = new StringBuilder();

			Argument arg1 = tuple.getArg1();
			decoration.append(StringUtils.repeat(" ", arg1.getBegin())
					+ StringUtils.repeat("-", arg1.getCoveredText().length()));

			Relation rel = tuple.getRelation();

			decoration.append(StringUtils.repeat(" ", rel.getBegin() - decoration.length())
					+ StringUtils.repeat("~", rel.getCoveredText().length()));

			Argument arg2 = tuple.getArg2();
			decoration.append(StringUtils.repeat(" ", arg2.getBegin() - decoration.length())
					+ StringUtils.repeat("=", arg2.getCoveredText().length()));

			System.out.println(decoration.toString());
			System.out.println();
		}

	}

}
